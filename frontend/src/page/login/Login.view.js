import React, {Component} from 'react';
import {Avatar, Button, Container, Grid, Link, Typography, TextField} from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import CircularProgress from '@material-ui/core/CircularProgress';

import Style from "./Login.style";

export default class LoginView extends Component{
    renderSubmit(props){
        return props.showLoad ? (
            <Button style={Style.submit} variant="contained" color="primary" type="submit" disabled={true} fullWidth>
                <CircularProgress color="primary" size={24} />
            </Button>
        ) : (
            <Button style={Style.submit} variant="contained" color="primary" type="submit" fullWidth>
                Sign In
            </Button>
        )
    }

    render() {
        return (
            <Container component="main" maxWidth="xs" style={Style.container}>
                <div style={Style.paper}>
                    <Avatar style={Style.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5" style={Style.label}>Sign in</Typography>

                    <form style={Style.form} onSubmit={this.props.onSubmit}>
                        <TextField
                            id="email" name="email" margin="normal" variant="outlined"
                            label="Email Address" required fullWidth autoFocus
                            onChange={this.props.onChangeForm} />

                        <TextField
                            id="password" name="password" margin="normal" variant="outlined"
                            label="Password" type="password" fullWidth required
                            onChange={this.props.onChangeForm} />

                        {this.renderSubmit(this.props)}

                        <Grid container>
                            <Grid item xs>
                                <Link href="#" variant="body2">
                                    Forgot password?
                                </Link>
                            </Grid>
                            <Grid item>
                                <Link href="/register" variant="body2">
                                    {"Don't have an account? Sign Up"}
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
        )
    }
}