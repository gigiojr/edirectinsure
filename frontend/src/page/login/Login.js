import React, {Component} from 'react';
import {Redirect} from "react-router-dom";

import {RestAPI} from "../../module";
import LoginView from "./Login.view";

export default class Login extends Component{
    constructor(props) {
        super(props);

        this.state = {
            redirectToMain: false,
            showLoad: false,
            form: {
                email: "",
                password: ""
            }
        }
    }

    onChangeForm(e){
        e.preventDefault();
        let {form} = this.state;
        form[e.target.id] = e.target.value;
        this.setState({form});
    }

    onSubmit(e){
        e.preventDefault();
        this.setState({showLoad: true});
        let {form} = this.state;
        // form.password = Encryption.encryptByAES(form.password, form.email);

        RestAPI.login(form.email, form.password).then((res) => {
            if(res.svStatus){
                this.setState({showLoad: false, redirectToMain: true, user: res.data});
            } else{
                let msg = res.error.message || "Credentials is not valid to authenticate.";
                this.setState({showLoad: false}, () => alert(msg));
            }
        }).catch((e) => {
            console.error(e);
            this.setState({showLoad: false});
        });
    }

    render() {
        return this.state.redirectToMain ? (
            <Redirect to={`/${this.state.user.idUser}`} />
        ) : (
            <LoginView
                showLoad={this.state.showLoad}
                onChangeForm={this.onChangeForm.bind(this)}
                onSubmit={this.onSubmit.bind(this)} />
        )
    }
}