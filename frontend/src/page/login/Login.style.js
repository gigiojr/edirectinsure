export default {
    container: {
        flex: 1,
        marginTop: 32
    },
    paper: {
        marginTop: 8,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: "center",
    },
    avatar: {
        marginRight: "auto",
        marginLeft: "auto",
        backgroundColor: "grey",
    },
    label: {
        textAlign: "center"
    },
    form: {
        width: '100%',
        marginTop: 1,
    },
    submit: {
        margin: "3, 0, 2",
    },
};