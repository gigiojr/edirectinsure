export default {
    container: {
        flex: 1,
        marginTop: 32
    },
    toolbar: {
        justifyContent: "space-between",
        backgroundColor: "#eeeeee"
    },
    menu: {
        marginTop: 40,
        marginLeft: 24
    },
};