import React, {Component} from 'react';
import {Redirect} from "react-router-dom";

import {RestAPI} from "../../module";

import MainView from "./Main.view";

export default class Main extends Component{
    constructor(props) {
        super(props);

        this.state = {
            showLoad: false,
            redirectToLogin: false,

            idUser: this.props.match.params.idUser,

            openProjectDialog: false,
            openTaskDialog: false,

            menuOpen: false,
            menuAnchorEl: null,

            projectList: [],
            taskList: [],
            taskHash: {},
            user: null,

            projectSelected: null,
            taskSelected: null,

            form: {
                projectName: "",
                taskName: "",
                taskDescription: "",
            }
        }
    }

    componentDidMount() {
        this.getProjects();
    }

    getProjects(){
        RestAPI.getProjects(this.state.idUser).then((res) => {
            if(res.svStatus){
                this.getProjectsTasks(res.data);
            }
        }).catch((e) => {
            console.error(e);
            alert("It was not possible to get your projects. Try again later.");
            this.setState({redirectToLogin: true});
        });
    }

    getProjectsTasks(projectList){
        if(projectList.length > 0){
            let idList = projectList.map((p) => `idProject=${p.idProject}`).join("&");
            RestAPI.getTasks(idList).then((res) => {
                if(res.svStatus){
                    let taskHash = {}, taskList = res.data;
                    taskList.forEach((t) => {
                        if(taskHash[t.idProject] && taskHash[t.idProject].length > 0)
                            taskHash[t.idProject].push(t);
                        else
                            taskHash[t.idProject] = [t];
                    });
                    this.setState({projectList, taskHash, taskList});
                }
            }).catch((e) => {
                console.error(e);
                alert("It was not possible to get your projects. Try again later.");
                this.setState({redirectToLogin: true});
            });
        } else{
            this.setState({projectList});
        }
    }

    handleMenu = (event) => this.setState({menuOpen: !this.state.menuOpen, menuAnchorEl: event.currentTarget});
    handleClose = () => this.setState({menuAnchorEl: null});

    onChangeForm(e){
        console.log(e.target.id, e.target.value)
        let {form} = this.state;
        form[e.target.id] = e.target.value;
        this.setState({form});
    }

    onLogout(e){
        e.preventDefault();
        this.setState({redirectToLogin: true});
    }

    onProjectDelete(project, idx){
        this.setState({showLoad: true});
        RestAPI.deleteProject(project.idProject).then((res) => {
            if(res.svStatus){
                let {projectList} = this.state;
                projectList.splice(idx, 1);
                this.setState({showLoad: false, projectList});
            }
        }).catch((e) => {
            console.error(e);
            alert("There was a problem with request. Try again later.");
        });
    }

    onProjectEdit(project){
        let {form} = this.state;
        form.projectName = project.name || "";
        this.setState({openProjectDialog: true, projectSelected: project, form});
    }

    onSubmitEditProject(e) {
        e.preventDefault();

        let {form, projectList, projectSelected} = this.state;
        projectSelected.name = form.projectName;

        RestAPI.setProject(projectSelected).then((res) => {
            if(res.svStatus){
                let idx = projectList.findIndex((p) => p.idProject === projectSelected.idProject);
                projectList[idx] = res.data[0];
                form.projectName = "";
                this.setState({projectList, projectSelected: null, openProjectDialog: false, form});
            }
        }).catch((e) => {
            console.error(e);
            alert("There was a problem with request. Try again later.");
        });
    }

    onSubmitEditTask(e) {
        e.preventDefault();
        let {form, taskSelected} = this.state;
        taskSelected.name = form.taskName;
        taskSelected.description = form.taskDescription;
        this.onTaskEdit(taskSelected);
    }

    onSubmitNewProject(project){
        project.idUser = this.state.idUser;
        this.setState({showLoad: true});
        RestAPI.addProject(project).then((res) => {
            if(res.svStatus){
                let {projectList} = this.state;
                projectList.push(res.data[0]);
                this.setState({showLoad: false, projectList});
            }
        }).catch((e) => {
            console.error(e);
            alert("There was a problem with request. Try again later.");
        });
    }

    onSubmitNewTask(task){
        this.setState({showLoad: true});
        RestAPI.addTask(task).then((res) => {
            if(res.svStatus){
                let {taskHash} = this.state;
                if(taskHash[task.idProject]){
                    taskHash[task.idProject].push(res.data[0]);
                } else{
                    taskHash[task.idProject] = [res.data[0]];
                }
                this.setState({showLoad: false, taskHash});
            }
        }).catch((e) => {
            console.error(e);
            alert("There was a problem with request. Try again later.");
        });
    }

    onTaskClick(task){
        let {form} = this.state;
        form.taskName = task.name || "";
        form.taskDescription = task.description || "";
        this.setState({openTaskDialog: true, taskSelected: task, form});
    }

    onTaskDelete(task){
        RestAPI.deleteTask(task.idTask).then((res) => {
            if(res.svStatus){
                let taskHash = {...this.state.taskHash};
                let idx = taskHash[task.idProject].findIndex((t) => t.idTask === task.idTask);
                taskHash[task.idProject].splice(idx, 1);
                this.setState({taskHash});
            }
        }).catch((e) => {
            console.error(e);
            alert("There was a problem with request. Try again later.");
        });
    }

    onTaskEdit(task){
        RestAPI.setTask(task).then((res) => {
            if(res.svStatus){
                let {form} = this.state;
                form.taskName = "";
                form.taskDescription = "";
                let taskHash = {...this.state.taskHash};
                taskHash[task.idProject] = taskHash[task.idProject].map((t) => t.idTask === task.idTask ? res.data[0] : t);
                this.setState({openTaskDialog: false, taskHash, form});
            }
        }).catch((e) => {
            console.error(e);
            alert("There was a problem with request. Try again later.");
        });
    }

    render() {
        return this.state.redirectToLogin ? (
            <Redirect to="/login" />
        ) : (
            <MainView
                form={this.state.form}
                menuOpen={this.state.menuOpen}
                menuAnchor={this.state.menuAnchorEl}
                onChangeForm={this.onChangeForm.bind(this)}
                onCloseProjectDialog={() => this.setState({openProjectDialog: false, projectSelected: null})}
                onCloseTaskDialog={() => this.setState({openTaskDialog: false, taskSelected: null})}
                onMenuOpen={this.handleMenu.bind(this)}
                onMenuClose={this.handleClose.bind(this)}
                onLogout={this.onLogout.bind(this)}
                onProjectDelete={this.onProjectDelete.bind(this)}
                onProjectEdit={this.onProjectEdit.bind(this)}
                onSubmitEditProject={this.onSubmitEditProject.bind(this)}
                onSubmitEditTask={this.onSubmitEditTask.bind(this)}
                onSubmitNewProject={this.onSubmitNewProject.bind(this)}
                onNewTask={this.onSubmitNewTask.bind(this)}
                onTaskClick={this.onTaskClick.bind(this)}
                onTaskDelete={this.onTaskDelete.bind(this)}
                onTaskEdit={this.onTaskEdit.bind(this)}
                openProjectDialog={this.state.openProjectDialog}
                openTaskDialog={this.state.openTaskDialog}
                projectList={this.state.projectList}
                projectSelected={this.state.projectSelected}
                showLoad={this.state.showLoad}
                taskHash={this.state.taskHash}
                taskSelected={this.state.taskSelected}
                userName={"tes"} />
        )
    }
}