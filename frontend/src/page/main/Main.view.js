import React, {Component} from 'react';
import {
    Button,
    Card,
    Container,
    Dialog,
    DialogTitle,
    DialogActions,
    DialogContent,
    DialogContentText,
    Grid,
    GridList,
    Menu,
    MenuItem,
    TextField,
    Toolbar,
    Typography
} from '@material-ui/core';
import moment from "moment";

import {FormNewProject, ProjectItem} from "../../component";

import Style from "./Main.style";

export default class MainView extends Component{
    renderHeader(props){
        return (
            <Toolbar style={Style.toolbar}>
                <Typography component="h1" variant="h5">EDirectInsure TODO List</Typography>

                <Button onClick={props.onMenuOpen}>
                    {props.userName}
                    <span className="material-icons">arrow_drop_down</span>
                    <Menu
                        anchorEl={props.menuAnchor}
                        id="menu-appbar"
                        onClose={props.onMenuClose}
                        open={props.menuOpen}
                        style={Style.menu}>

                        <MenuItem onClick={props.onLogout}>Logout</MenuItem>
                    </Menu>
                </Button>
            </Toolbar>
        )
    }

    renderProjectDialog(props){
        if(!props.projectSelected)
            return null;

        return (
            <Dialog open={props.openProjectDialog} onClose={props.onCloseProjectDialog} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">{`Project: ${props.projectSelected.name}`}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {`Created: ${moment(props.projectSelected.createAt).format("DD/MM/YYYY HH:mm")}`}
                    </DialogContentText>
                    <TextField
                        autoFocus
                        id="projectName"
                        label="Project Name"
                        fullWidth
                        onChange={props.onChangeForm}
                        value={props.form.projectName}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={props.onCloseProjectDialog} color="primary">Cancel</Button>
                    <Button onClick={props.onSubmitEditProject} color="primary">Save</Button>
                </DialogActions>
            </Dialog>
        )
    }

    renderTaskDialog(props){
        if(!props.taskSelected)
            return null;

        let disabled = props.taskSelected.finishedAt !== null;
        return (
            <Dialog open={props.openTaskDialog} onClose={props.onCloseTaskDialog} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">{`Task: ${props.taskSelected.name}`}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {`Created: ${moment(props.taskSelected.createAt).format("DD/MM/YYYY HH:mm")}`}
                    </DialogContentText>
                    { disabled &&
                        <DialogContentText>
                            {`Finished: ${moment(props.taskSelected.finishedAt).format("DD/MM/YYYY HH:mm")}`}
                        </DialogContentText>
                    }
                    <TextField
                        autoFocus
                        id="taskName"
                        label="Task Name"
                        disabled={disabled}
                        fullWidth
                        onChange={props.onChangeForm}
                        value={props.form.taskName} />

                    <TextField
                        autoFocus
                        id="taskDescription"
                        label="Description"
                        disabled={disabled}
                        fullWidth
                        onChange={props.onChangeForm}
                        value={props.form.taskDescription} />
                </DialogContent>
                <DialogActions>
                    <Button onClick={props.onCloseTaskDialog} color="primary">Cancel</Button>
                    {!disabled && <Button onClick={props.onSubmitEditTask} color="primary">Save</Button>}
                </DialogActions>
            </Dialog>
        )
    }

    render() {
        return (
            <Container style={Style.container} component="main" maxWidth={false}>
                {this.renderProjectDialog(this.props)}
                {this.renderTaskDialog(this.props)}
                <Card>
                    {this.renderHeader(this.props)}

                    <GridList cellHeight="auto">
                        {this.props.projectList.map((p, idx) =>
                            <ProjectItem
                                key={idx} index={idx} project={p}
                                taskList={this.props.taskHash[p.idProject]}
                                onChangeForm={this.props.onChangeForm}
                                onProjectDelete={this.props.onProjectDelete}
                                onProjectEdit={this.props.onProjectEdit}
                                onNewTask={this.props.onNewTask}
                                onTaskClick={this.props.onTaskClick}
                                onTaskDelete={this.props.onTaskDelete}
                                onTaskEdit={this.props.onTaskEdit}
                                showLoad={this.props.showLoad}
                                style={{margin: 16, maxWidth: 400}} />
                        )}

                        <Grid>
                            <FormNewProject
                                onChange={this.props.onChangeForm}
                                onSubmit={this.props.onSubmitNewProject}
                                showLoad={this.props.showLoad} />
                        </Grid>
                    </GridList>
                </Card>
            </Container>
        )
    }
}