export default {
    container: {
        flex: 1,
        marginTop: 32
    },
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {

        marginRight: "auto",
        marginLeft: "auto",
        backgroundColor: "grey",
    },
    form: {
        width: '100%',
        marginTop: 16,
    },
    submit: {
        margin: "3, 0, 2",
    },
}