import React, {Component} from 'react';
import {Redirect} from "react-router-dom";

import RegisterView from "./Register.view";
import {RestAPI} from "../../module";

export default class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showLoad: false,
            redirectToLogin: false,
            form: {
                firstName: "",
                lastName: "",
                email: "",
                password: ""
            }
        }
    }

    onChangeForm(e){
        let {form} = this.state;
        form[e.target.id] = e.target.value;
        this.setState({form});
    }

    onSubmit(e){
        e.preventDefault();
        this.setState({showLoad: true});
        let {form} = this.state;

        RestAPI.newUser(form).then((res) => {
            console.log(res);
            if(res.svStatus){
                this.setState({showLoad: false, redirectToLogin: true});
            } else{
                alert("There was an error to insert a new user.");
            }
        }).catch((e) => {
            console.error(e);
            this.setState({showLoad: false});
            alert("There was an error to insert a new user.");
        });
    }

    render(){
        return this.state.redirectToLogin ? (
            <Redirect to="/login" />
        ) : (
            <RegisterView
                showLoad={this.state.showLoad}
                onChangeForm={this.onChangeForm.bind(this)}
                onSubmit={this.onSubmit.bind(this)} />
        )
    }
}