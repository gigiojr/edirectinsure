import React, {Component} from 'react';
import {
    Avatar, Button, Checkbox, Container, FormControlLabel,
    Grid, Link, Typography, TextField
} from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import CircularProgress from '@material-ui/core/CircularProgress';

import Style from "./Register.style";

export default class RegisterView extends Component{
    renderSubmit(props){
        return props.showLoad ? (
            <Button style={Style.submit} type="submit" color="primary" disabled={true} variant="contained" fullWidth>
                <CircularProgress color="primary" size={24} />
            </Button>
        ) : (
            <Button style={Style.submit} type="submit" color="primary" variant="contained" fullWidth>
                Sign Up
            </Button>
        )
    }

    render() {
        return (
            <Container style={Style.container} component="main" maxWidth="xs">
                <div style={Style.paper}>
                    <Avatar style={Style.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">Register</Typography>

                    <form style={Style.form} onSubmit={this.props.onSubmit}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id="firstName" name="firstName" variant="outlined"
                                    label="First Name" required fullWidth autoFocus
                                    onChange={this.props.onChangeForm} />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id="lastName" name="lastName" variant="outlined"
                                    label="Last Name" required fullWidth
                                    onChange={this.props.onChangeForm} />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    id="email" name="email" variant="outlined"
                                    label="Email Address" fullWidth required
                                    onChange={this.props.onChangeForm} />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    id="password" name="password" variant="outlined"
                                    type="password" label="Password" required fullWidth
                                    onChange={this.props.onChangeForm} />
                            </Grid>
                            <Grid item xs={12}>
                                <FormControlLabel
                                    control={<Checkbox value="allowExtraEmails" color="primary" required />}
                                    label="I agree with all terms." />
                            </Grid>
                        </Grid>

                        {this.renderSubmit(this.props)}

                        <Grid container justify="flex-end">
                            <Grid item>
                                <Link href="/login" variant="body2">
                                    Already have an account? Sign in
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
        )
    }
}