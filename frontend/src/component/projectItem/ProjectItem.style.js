export default {
    card: {
        margin: 24,
        width: "auto",
    },
    divisor: {
        height: 1,
        marginTop: 16,
        marginLeft: 24,
        marginRight: 24,
        backgroundColor: "#eeeeee"
    },
    formGrid: {
        paddingLeft: 24,
        paddingRight: 24,
        marginTop: 16,
        marginBottom: 16
    },
    subtitleLabel: {
        marginLeft: 24,
        marginRight: 24
    },
    toolbar: {
        justifyContent: "space-between",
        backgroundColor: "#eeeeee"
    },
    submit: {
        color: "white",
        backgroundColor: "#02b8d9",
    },
}