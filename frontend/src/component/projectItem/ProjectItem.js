import React, {PureComponent} from 'react';

import ProjectItemView from "./ProjectItem.view";

export default class ProjectItem extends PureComponent{
    constructor(props) {
        super(props);

        this.state = {
            form: {
                taskName: ""
            }
        }
    }

    onChangeForm(e){
        let {form} = this.state;
        form[e.target.id] = e.target.value;
        this.setState({form});
    }

    onDeleteProject(e){
        e.preventDefault();
        if(this.props.onDeleteProject)
            this.props.onDeleteProject(e);
    }

    onNewTask(e){
        e.preventDefault();
        let {form} = this.state;
        if(this.props.onNewTask)
            this.props.onNewTask({idProject: this.props.project.idProject, name: form.taskName});

        form.projectName = "";
        document.getElementById(`new-task-form-${this.props.project.idProject}`).reset();
        this.setState({form});
    }

    onProjectDelete(e){
        e.preventDefault();
        let {project, index} = this.props;
        if(this.props.onProjectDelete)
            this.props.onProjectDelete(project, index);
    }

    onProjectEdit(e){
        e.preventDefault();
        let {project, index} = this.props;
        if(this.props.onProjectEdit)
            this.props.onProjectEdit(project, index);
    }

    render(){
        return (
            <ProjectItemView
                project={this.props.project}
                taskList={this.props.taskList}
                onChangeForm={this.onChangeForm.bind(this)}
                onProjectDelete={this.onProjectDelete.bind(this)}
                onProjectEdit={this.onProjectEdit.bind(this)}
                onNewTask={this.onNewTask.bind(this)}
                onTaskClick={this.props.onTaskClick}
                onTaskDelete={this.props.onTaskDelete}
                onTaskEdit={this.props.onTaskEdit}
                showLoad={this.props.showLoad}
                taskName={this.state.form.taskName} />
        )
    }
}