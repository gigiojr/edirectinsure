import React, {PureComponent} from 'react';
import {
    Button,
    Card,
    Grid,
    TextField,
    Toolbar,
    Typography
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";

import TaskItem from "../taskItem/TaskItem";

import Style from "./ProjectItem.style";

export default class ProjectItemView extends PureComponent{

    renderHeader(props){
        return(
            <Toolbar style={Style.toolbar}>
                <Typography component="h1" variant="h6">{props.project.name}</Typography>

                <div>
                    <EditIcon style={{cursor: "pointer"}} onClick={props.onProjectEdit}  />
                    <DeleteIcon style={{cursor: "pointer", marginLeft: 16}} onClick={props.onProjectDelete} />
                </div>
            </Toolbar>
        )
    }

    renderToDo(props, taskList){
        return (
            <Grid>
                {taskList.length > 0 ? taskList.map((t, idx) =>
                    <TaskItem
                        key={idx}
                        onTaskClick={props.onTaskClick}
                        onTaskDelete={props.onTaskDelete}
                        onTaskEdit={props.onTaskEdit}
                        showDelete={true}
                        task={t} />
                ) : (
                    <p style={{marginLeft: 40}}>Empty</p>
                )}
            </Grid>
        )
    }

    renderDone(props, taskList){
        return (
            <Grid>
                {taskList.length > 0 ? taskList.map((t, idx) =>
                    <TaskItem
                        key={idx}
                        onTaskClick={props.onTaskClick}
                        onTaskDelete={props.onTaskDelete}
                        onTaskEdit={props.onTaskEdit}
                        task={t} />
                ) : (
                    <p style={{marginLeft: 40}}>Empty</p>
                )}
            </Grid>
        )
    }

    renderFormTask(props){
        return (
            <form id={`new-task-form-${props.project.idProject}`} style={Style.form} onSubmit={props.onNewTask}>
                <Grid container spacing={2} style={Style.formGrid}>
                    <Grid item>
                        <TextField
                            id="taskName" name="taskName" variant="outlined"
                            label="Task" size="small" required fullWidth
                            onChange={props.onChangeForm} />
                    </Grid>
                    <Grid item>
                        <Button style={Style.submit} type="submit" color="primary" variant="contained">
                            Add
                        </Button>
                    </Grid>
                </Grid>
            </form>
        )
    }

    render(){
        let openTaskList = [];
        let doneTaskList = [];

        if(this.props.taskList) {
            this.props.taskList.forEach((t) => {
                if (t.finishedAt) {
                    doneTaskList.push(t);
                } else {
                    openTaskList.push(t);
                }
            });
        }

        return (
            <Card style={{...Style.card, ...this.props.style}}>
                {this.renderHeader(this.props)}

                <Typography style={Style.subtitleLabel} component="h1" variant="h6">To Do</Typography>
                {this.renderToDo(this.props, openTaskList)}

                <Typography style={Style.subtitleLabel} component="h1" variant="h6">Done</Typography>
                {this.renderDone(this.props, doneTaskList)}

                <div style={Style.divisor} />
                {this.renderFormTask(this.props)}
            </Card>
        )
    }
}