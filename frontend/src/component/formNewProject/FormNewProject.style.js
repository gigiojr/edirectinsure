export default {
    newProjectTitle: {
        textAlign: "center"
    },
    paper: {
        borderRadius: 8,
        margin: 16,
        padding: 32,
        width: 300,
        height: 150,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: "center",
        backgroundColor: "#eeeeee"
    },
    form: {
        backgroundColor: "#eeeeee"
    },
    field: {
        backgroundColor: "#fff"
    },
    submit: {
        color: "white",
        backgroundColor: "#02b8d9",
    },
}