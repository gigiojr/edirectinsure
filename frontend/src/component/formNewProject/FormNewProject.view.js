import React, {PureComponent} from 'react';
import {
    Button,
    TextField,
    Typography
} from "@material-ui/core";

import Style from "./FormNewProject.style";

export default class FormNewProjectView extends PureComponent{
    render(){
        return (
            <div style={{...Style.paper, ...this.props.style}}>
                <Typography style={Style.newProjectTitle} component="h1" variant="h5">
                    Create a new project
                </Typography>

                <form id="project-form" style={Style.form} onSubmit={this.props.onSubmit}>
                    <TextField
                        style={Style.field} required
                        id="projectName" name="projectName" margin="normal" variant="outlined"
                        label="Project name" color="primary" size="small" fullWidth
                        onChange={this.props.onChangeForm} />

                    <Button style={Style.submit} variant="contained" type="submit" fullWidth>
                        Create Project
                    </Button>
                </form>
            </div>
        )
    }
}