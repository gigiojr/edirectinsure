import React, {PureComponent} from 'react';

import FormNewProjectView from "./FormNewProject.view";

export default class FormNewProject extends PureComponent{
    constructor(props) {
        super(props);

        this.state = {
            form: {
                projectName: ""
            }
        }
    }

    onChangeForm(e){
        let {form} = this.state;
        form[e.target.id] = e.target.value;
        this.setState({form});
    }

    onSubmit(e){
        e.preventDefault();
        let {form} = this.state;
        if(this.props.onSubmit)
            this.props.onSubmit({name: form.projectName});

        form.projectName = "";
        document.getElementById("project-form").reset();
        this.setState({form});
    }

    render(){
        return (
            <FormNewProjectView
                onSubmit={this.onSubmit.bind(this)}
                onChangeForm={this.onChangeForm.bind(this)}
                projectName={this.state.form.projectName}
                showLoad={this.props.showLoad} />
        )
    }
}