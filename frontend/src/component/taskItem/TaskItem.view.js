import React, {PureComponent} from 'react';
import {Checkbox, Grid, Link, Tooltip} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import moment from "moment";

import Style from "./TaskItem.style";

export default class TaskItemView extends PureComponent{

    renderDeleteIcon(props){
        return props.showDelete && <DeleteIcon style={Style.todoDeleteIcon} onClick={props.onTaskDelete} />
    }

    render(){
        let {task} = this.props;
        let tooltip = task.finishedAt ?
            `Created at ${moment(task.createAt).format("DD/MM/YYYY HH:mm")} and
            finished at ${moment(task.finishedAt).format("DD/MM/YYYY HH:mm")}` :
            `Created at ${moment(task.createAt).format("DD/MM/YYYY HH:mm")}`;
        return (
            <Grid style={Style.todoGrid}>
                <div style={{flexDirection: "row"}}>
                    <Checkbox
                        color="primary"
                        checked={this.props.task.finishedAt !== null}
                        onClick={this.props.onTaskEdit}
                        value={!this.props.task.finishAt}  />

                    <Tooltip title={tooltip}>
                        <Link style={{cursor: "pointer"}} onClick={this.props.onTaskClick}>{this.props.task.name}</Link>
                    </Tooltip>
                </div>

                {this.renderDeleteIcon(this.props)}
            </Grid>
        )
    }
}