export default {
    todoGrid: {
        display: "flex",
        flexDirection: "row",
        marginLeft: 32,
        marginRight: 16,
        width: "100%"
    },
    todoDeleteIcon: {
        fontSize: 24,
        marginLeft: 16,
        marginRight: 16,
        alignSelf: "center"
    }
}