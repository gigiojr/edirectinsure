import React, {PureComponent} from 'react';
import moment from "moment";

import TaskItemView from "./TaskItem.view";

export default class TaskItem extends PureComponent{
    constructor(props) {
        super(props);

        this.state = {
        }
    }

    onClick(){
        if(this.props.onTaskClick){
            this.props.onTaskClick(this.props.task);
        }
    }

    onCloseDialog(){
        this.setState({
            openDialog: false
        })
    }

    onDelete(){
        if(this.props.onTaskDelete){
            this.props.onTaskDelete(this.props.task);
        }
    }

    onEdit(e){
        e.preventDefault();
        let task = this.props.task;
        task.finishedAt = !task.finishedAt ? new moment().toDate() : null;
        if(this.props.onTaskEdit){
            this.props.onTaskEdit(task);
        }
    }

    render(){
        return (
            <TaskItemView
                onCloseDialog={this.onCloseDialog.bind(this)}
                onTaskClick={this.onClick.bind(this)}
                onTaskDelete={this.onDelete.bind(this)}
                onTaskEdit={this.onEdit.bind(this)}
                showDelete={this.props.showDelete}
                task={this.props.task} />
        )
    }
}