import React from "react";
import {SimpleDialog, PromptDialog, ConfirmDialog} from "./";

export default class Dialog extends React.Component {

	static SIMPLE_DIALOG = 1;
	static PROMPT_DIALOG = 2;
    static CONFIRM_DIALOG = 3;

    render() {
        if(!this.props.type || !this.props.dialogOptions) return;
        return (
            <React.Fragment>
                {this.props.type === Dialog.SIMPLE_DIALOG ? (
                    <SimpleDialog {...this.props.dialogOptions}/>
                ) : this.props.type === Dialog.PROMPT_DIALOG ? (
                    <PromptDialog {...this.props.dialogOptions}/>
                ) : this.props.type === Dialog.CONFIRM_DIALOG && (
                    <ConfirmDialog {...this.props.dialogOptions}/>
                )}
            </React.Fragment>
        )
    };
}