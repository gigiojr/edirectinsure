import Login from "./page/login/Login";
import Main from "./page/main/Main";
import Register from "./page/register/Register";

export default [{
    path: "/login",
    exact: true,
    component: Login
},{
    path: "/register",
    exact: true,
    component: Register
},{
    path: "/:idUser",
    exact: true,
    component: Main
}]