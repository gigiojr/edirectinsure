export default class RestAPI{
    static URL = "http://localhost:3000";

    static httpMethod(method, url, body) {
        console.log("url", method, url)
        return fetch(url, {
            method,
            headers: {
                'Content-Type': 'application/json',
                // Authorization: `Bearer ${Auth.getToken()}`
            },
            body: JSON.stringify(body)
        }).then((res) => res.json());
    }

    static httpGet = (url) => RestAPI.httpMethod("GET", url);
    static httpPost = (url, body) => RestAPI.httpMethod("POST", url, body);
    static httpPut = (url, body) => RestAPI.httpMethod("PUT", url, body);
    static httpDelete = (url) => RestAPI.httpMethod("DELETE", url);

    // ************************************************** //
    // Project
    // ************************************************** //
    static URL_PROJECT = `${RestAPI.URL}/project`;

    static addProject = (project) => this.httpPost(`${this.URL_PROJECT}`, project);
    static getProjects = (user) => this.httpGet(`${this.URL_PROJECT}?idUser=${user}`);
    static setProject = (project) => this.httpPut(`${this.URL_PROJECT}`, project);
    static deleteProject = (project) => this.httpDelete(`${this.URL_PROJECT}?idProject=${project}`);

    // ************************************************** //
    // Task
    // ************************************************** //
    static URL_TASK = `${RestAPI.URL}/task`;

    static addTask = (task) => this.httpPost(`${this.URL_TASK}`, task);
    static getTasks = (params) => this.httpGet(`${this.URL_TASK}?${params}`);
    static setTask = (task) => this.httpPut(`${this.URL_TASK}`, task);
    static deleteTask = (task) => this.httpDelete(`${this.URL_TASK}?idTask=${task}`);

    // ************************************************** //
    // User Controller
    // ************************************************** //
    static URL_USER = `${RestAPI.URL}/user`;

    static login = (email, password) => this.httpGet(`${this.URL_USER}/login?email=${email}&password=${password}`);
    static logout = (email) => this.httpGet(`${this.URL_USER}/logout?email=${email}`);
    static newUser = (user) => this.httpPost(`${this.URL_USER}`, user);
}
