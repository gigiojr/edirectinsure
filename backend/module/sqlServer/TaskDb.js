const SqlDb = require("./SQLServer");

const tblTask = {
    table: "[dbo].[tblTask]",
    col: {
        idTask: "idTask",
        idProject: "idProject",
        name: "name",
        description: "description",
        createAt: "createAt",
        finishedAt: "finishedAt",
    }
};

function AddTask(task){
    return new Promise((resolve, reject) => {
        if(!ValidTask(task))
            return reject("Project object is not valid to insert.")

        let col = tblTask.col;
        let sql = `INSERT INTO ${tblTask.table} (${col.idProject}, ${col.name}) OUTPUT INSERTED.*
            VALUES ('${task[col.idProject]}', '${task[col.name]}')`;

        SqlDb.ExecSQL(sql, GetTaskByColumns)
            .then((data) => resolve(data))
            .catch((e) => reject(e));
    });
}

function DeleteTask(idTask){
    return new Promise((resolve, reject) => {
        if(idTask <= 0)
            return reject("Required param is not valid to insert.")

        let col = tblTask.col;
        let sql = `DELETE ${tblTask.table} OUTPUT DELETED.* WHERE ${col.idTask} = ${idTask}`;

        SqlDb.ExecSQL(sql, GetTaskByColumns)
            .then((data) => resolve(data))
            .catch((e) => reject(e));
    });
}

function DeleteTaskByProject(idProject){
    return new Promise((resolve, reject) => {
        if(idProject <= 0)
            return reject("Required param is not valid to delete.")

        let col = tblTask.col;
        let sql = `DELETE ${tblTask.table} OUTPUT DELETED.* WHERE ${col.idProject} = ${idProject}`;

        SqlDb.ExecSQL(sql, GetTaskByColumns)
            .then((data) => resolve(data))
            .catch((e) => reject(e));
    });
}

function GetTasksByProjects(idProjectList){
    return new Promise((resolve, reject) => {
        if(!idProjectList || idProjectList.length <= 0)
            return reject("A require param is not available.")

        let col = tblTask.col;
        let sql = `SELECT * FROM ${tblTask.table} WHERE ${col.idProject} IN (${idProjectList})`;

        SqlDb.ExecSQL(sql, GetTaskByColumns)
            .then((data) => resolve(data))
            .catch((e) => reject(e));
    });
}

function SetTask(task){
    return new Promise((resolve, reject) => {
        if(!ValidTask(task, true))
            return reject("Task object is not valid to update.")

        let col = tblTask.col;
        let description = task[col.description] ? `'${task[col.description]}'` : null;
        let finishedAt = task[col.finishedAt] ? `'${task[col.finishedAt]}'` : null;
        let sql = `UPDATE ${tblTask.table} 
            SET ${col.name} = '${task[col.name]}',
                ${col.description} = ${description},
                ${col.finishedAt} = ${finishedAt}
            OUTPUT INSERTED.*
            WHERE ${col.idTask} = ${task[col.idTask]}`;

        SqlDb.ExecSQL(sql, GetTaskByColumns)
            .then((data) => resolve(data))
            .catch((e) => reject(e));
    });
}

function GetTaskByColumns(columns){
    return {
        idTask: columns[0].value,
        idProject: columns[1].value,
        name: columns[2].value,
        description: columns[3].value,
        createAt: columns[4].value,
        finishedAt: columns[5].value,
    };
}

function ValidTask(obj, validId = false){
    let col = tblTask.col;
    return obj[col.idProject] && obj[col.name] && (!validId || obj[col.idTask])
}

module.exports = {
    AddTask,
    DeleteTask,
    DeleteTaskByProject,
    GetTaskByColumns,
    GetTasksByProjects,
    SetTask,
    Table: tblTask,
    ValidTask,
}