const {Connection, Request} = require("tedious");

const ConfigConnection = {
    authentication: {
        options: {
            userName: "sa",
            password: "root"
        },
        type: "default"
    },
    server: `localhost`,
    options: {
        port: 49175,
        database: "EDirectInsure",
        instancename: 'SQLEXPRESS',
        encrypt: false
    }
};

function ExecSQL(sql, map){
    return new Promise((resolve, reject) => {
        let connection = new Connection(ConfigConnection);
        connection.on("connect", err => {
            if (err) {
                console.error(err.message);
                reject(err);
            } else{
                let request = new Request(sql, (err, rowCount) => {
                    if (err) {
                        console.error(err.message);
                        reject(err);
                    }
                    connection.close();
                });

                let list = [];
                request.on("row", columns => map ? list.push(map(columns)) : list.push(columns));
                request.on("requestCompleted", () => resolve(list));
                connection.execSql(request);
            }
        });
    });
}

module.exports = {
    ExecSQL
};