const SqlDb = require("./SQLServer");

const tblUser = {
    table: "[dbo].[tblUser]",
    col: {
        idUser: "idUser",
        email: "email",
        firstName: "firstName",
        lastName: "lastName",
        password: "password"
    }
};

function AddUser(user){
    return new Promise((resolve, reject) => {
        if(!ValidUser(user))
            return reject("User object is not valid to insert.")

        let col = tblUser.col;
        let sql = `INSERT INTO ${tblUser.table} (${col.email}, ${col.firstName}, ${col.lastName}, ${col.password})
        OUTPUT INSERTED.*
        VALUES ('${user[col.email]}', '${user[col.firstName]}', '${user[col.lastName]}', '${user[col.password]}')`;

        SqlDb.ExecSQL(sql, GetUserByColumns)
            .then((data) => resolve(data))
            .catch((e) => reject(e));
    });
}

function AuthenticateUser(email, password){
    return new Promise((resolve, reject) => {
        let col = tblUser.col;
        let sql = `SELECT * FROM ${tblUser.table} WHERE ${col.email} = '${email}' AND ${col.password} = '${password}'`;

        SqlDb.ExecSQL(sql, GetUserByColumns)
            .then((data) => {
                if(data && data.length === 1){
                    resolve(data[0]);
                } else{
                    reject("User is not be authenticate with this credentials.");
                }
            }).catch((e) => reject(e));
    });
}

function GetUserByColumns(columns){
    return {
        idUser: columns[0].value,
        email: columns[1].value,
        firstName: columns[2].value,
        lastName: columns[3].value,
        // password: columns[4].value,
    };
}

function ValidUser(obj, validId = false){
    let col = tblUser.col;
    return obj[col.email] && obj[col.firstName] && obj[col.lastName] &&
        obj[col.password] && (!validId || obj[col.idUser])
}

module.exports = {
    AddUser,
    AuthenticateUser,
    GetUserByColumns,
    Table: tblUser,
    ValidUser,
}