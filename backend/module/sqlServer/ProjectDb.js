const SqlDb = require("./SQLServer");
const TaskDb = require("./TaskDb");

const tblProject = {
    table: "[dbo].[tblProject]",
    col: {
        idProject: "idProject",
        idUser: "idUser",
        name: "name",
        createAt: "createAt",
    }
};

function AddProject(project){
    return new Promise((resolve, reject) => {
        if(!ValidProject(project))
            return reject("Project object is not valid to insert.")

        let col = tblProject.col;
        let sql = `INSERT INTO ${tblProject.table} (${col.idUser}, ${col.name}) OUTPUT INSERTED.*
            VALUES ('${project[col.idUser]}', '${project[col.name]}')`;

        SqlDb.ExecSQL(sql, GetProjectByColumns)
            .then((data) => resolve(data))
            .catch((e) => reject(e));
    });
}

function DeleteProject(idProject){
    return new Promise((resolve, reject) => {
        if(idProject <= 0)
            return reject("A require param is not available.")

        TaskDb.DeleteTaskByProject(idProject).then(() => {
            let col = tblProject.col;
            let sql = `DELETE ${tblProject.table} OUTPUT DELETED.* WHERE ${col.idProject} = ${idProject}`;

            SqlDb.ExecSQL(sql, GetProjectByColumns)
                .then((data) => resolve(data))
                .catch((e) => reject(e));
        }).catch((e) => reject(e));
    });
}

function GetProjectsByUser(idUser){
    return new Promise((resolve, reject) => {
        if(idUser <= 0)
            return reject("A require param is not available.")

        let col = tblProject.col;
        let sql = `SELECT * FROM ${tblProject.table} WHERE ${col.idUser} = ${idUser}`;

        SqlDb.ExecSQL(sql, GetProjectByColumns)
            .then((data) => resolve(data))
            .catch((e) => reject(e));
    });
}

function SetProject(project){
    return new Promise((resolve, reject) => {
        if(!ValidProject(project, true))
            return reject("Project object is not valid to update.")

        let col = tblProject.col;
        let sql = `UPDATE ${tblProject.table} SET ${col.name} = '${project[col.name]}' OUTPUT INSERTED.*
            WHERE ${col.idProject} = ${project[col.idProject]}`;

        SqlDb.ExecSQL(sql, GetProjectByColumns)
            .then((data) => resolve(data))
            .catch((e) => reject(e));
    });
}

function GetProjectByColumns(columns){
    return {
        idProject: columns[0].value,
        idUser: columns[1].value,
        name: columns[2].value,
        createAt: columns[3].value,
    };
}

function ValidProject(obj, validId = false){
    let col = tblProject.col;
    return obj[col.idUser] && obj[col.name] && (!validId || obj[col.idProject])
}

module.exports = {
    AddProject,
    DeleteProject,
    GetProjectByColumns,
    GetProjectsByUser,
    SetProject,
    Table: tblProject,
    ValidProject,
}