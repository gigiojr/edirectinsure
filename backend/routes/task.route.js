const router = require('express').Router();
const TaskDb = require('../module/sqlServer/TaskDb');

/* Get tasks by project. */
router.get('/', (req, res, next) => {
    let idProject = req.query.idProject;
    TaskDb.GetTasksByProjects(idProject)
        .then((data) => res.json({svStatus: true, data: data}))
        .catch((e) => res.json({svStatus: false, data: idProject, error: e}));
});

/* Create new task. */
router.post('/', (req, res, next) => {
    let body = req.body;
    TaskDb.AddTask(body)
        .then((data) => res.json({svStatus: true, data: data}))
        .catch((e) => res.json({svStatus: false, data: body, error: e}));
});

/* Edit task. */
router.put('/', (req, res, next) => {
    let body = req.body;
    TaskDb.SetTask(body)
        .then((data) => res.json({svStatus: true, data: data}))
        .catch((e) => res.json({svStatus: false, data: body, error: e}));
});

/* Delete task. */
router.delete('/', (req, res, next) => {
    let idTask = req.query.idTask;
    TaskDb.DeleteTask(idTask)
        .then((data) => res.json({svStatus: true, data: data}))
        .catch((e) => res.json({svStatus: false, data: idTask, error: e}));
});

module.exports = router;