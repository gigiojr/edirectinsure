const ProjectRoute = require("./project.route");
const TaskRoute = require("./task.route");
const UserRoute = require("./user.route");

module.exports = {
  ProjectRoute,
  TaskRoute,
  UserRoute
}