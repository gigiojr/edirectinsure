const router = require('express').Router();
const ProjectDb = require('../module/sqlServer/ProjectDb');

/* Create new project. */
router.get('/', (req, res, next) => {
    let idUser = req.query.idUser;
    ProjectDb.GetProjectsByUser(idUser)
        .then((data) => res.json({svStatus: true, data: data}))
        .catch((e) => res.json({svStatus: false, data: idUser, error: e}));
});

/* Create new project. */
router.post('/', (req, res, next) => {
    let body = req.body;
    ProjectDb.AddProject(body)
        .then((data) => res.json({svStatus: true, data: data}))
        .catch((e) => res.json({svStatus: false, data: body, error: e}));
});

/* Edit a project. */
router.put('/', (req, res, next) => {
    let body = req.body;
    ProjectDb.SetProject(body)
        .then((data) => res.json({svStatus: true, data: data}))
        .catch((e) => res.json({svStatus: false, data: body, error: e}));
});

/* Delete a project. */
router.delete('/', (req, res, next) => {
    let idProject = req.query.idProject;
    ProjectDb.DeleteProject(idProject)
        .then((data) => res.json({svStatus: true, data: data}))
        .catch((e) => res.json({svStatus: false, data: idProject, error: e}));
});

module.exports = router;