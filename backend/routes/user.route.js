const router = require('express').Router();
const UserDb = require('../module/sqlServer/UserDb');

/* Login user. */
router.get('/login', function(req, res, next) {
    let params = req.query;
    UserDb.AuthenticateUser(params.email, params.password)
        .then((data) => res.json({svStatus: true, data: data}))
        .catch((e) => res.json({svStatus: false, data: params, error: e}));
});

/* Create new user. */
router.post('/', function(req, res, next) {
    let body = req.body;
    UserDb.AddUser(body)
        .then((data) => res.json({svStatus: true, data: data}))
        .catch((e) => res.json({svStatus: false, data: body, error: e}));
});

/* Logout user. */
router.get('/logout', function(req, res, next) {
    res.json({svStatus: true});
});

module.exports = router;